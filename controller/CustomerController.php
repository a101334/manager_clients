<?php
    require_once __DIR__ . '/../model/DAO/CustomerDAO.php';

    class CustomerController {

        public function getAllCustomer() {
            $customerDAO = new CustomerDAO();
            return $customerDAO->getAllCustomer();
        }

        public function registerNewCustomer($customerArray) {
            if ($this->isValidFields($customerArray)) {
                $customerDAO = new CustomerDAO();
                return $customerDAO->registerNewCustomer($customerArray);
            } else {
                return 400;
            }
            
        }

        public function updateCustomer($customerArray, $cod) {
            if ($this->isValidFields($customerArray)) {
                $customerDAO = new CustomerDAO();
                $customerArray['cod'] = $cod;
                return $customerDAO->updateCustomer($customerArray);
            } else {
                return 400;
            }
        }

        public function deleteCustomer($cod) {
            
                if (isset($cod) && !is_null($cod) && !empty($cod)) {
                    $customerDAO = new CustomerDAO();
                    $customerDAO->deleteCustomer($cod);
                    return 200;
                } else {
                    return 400;
                }
        }

        private function isValidFields($arrayFields) {
            foreach($arrayFields as $value) {
                if (is_null($value) || empty($value)) {
                    return false;
                }
            }
            return true;
        }
    }
?>