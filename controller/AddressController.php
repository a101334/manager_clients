<?php
    require_once __DIR__ . '/../model/DAO/AddressDAO.php';

    class AddressController {

        public function getAllAddressCustomer($cod) {
            $addressDAO = new AddressDAO();
            return $addressDAO->getAllAddressCustomer($cod);
        }

        public function registerNewAddressCustomer($addressArray) {
            if ($this->isValidFields($addressArray)) {
                $addressDAO = new AddressDAO();
                return $addressDAO->registerNewAddressCustomer($addressArray); 
            } else {
                return 400;
            }
        }

        public function updateAddress($addressArray, $id) {
            if ($this->isValidFields($addressArray)) {
                $addressDAO = new AddressDAO();
                $addressArray['id'] = $id;
                return $addressDAO->updateAddressCustomer($addressArray);
            } else {
                return 400;
            }
        }

        public function deleteAddress($id) {
            
                if (isset($id) && !is_null($id) && !empty($id)) {
                    $addressDAO = new AddressDAO();
                    $addressDAO->deleteAddressCustomer($id);
                    return 200;
                } else {
                    return 400;
                }
        }

        private function isValidFields($arrayFields) {
            foreach($arrayFields as $key => $value) {
                if ($key != 'complement') {
                    if (is_null($value) || empty($value)) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
?>