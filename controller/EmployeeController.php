<?php
    session_start();
    require_once __DIR__ . '/../model/DAO/EmployeeDAO.php';
    class EmployeeController {

        public function tryLogin($arraySignIn) {
            if($this->isValidLogin(($arraySignIn))) {
                $employeeDAO = new EmployeeDAO();

                $result = $employeeDAO->signIn($arraySignIn);

                if(count($result) == 1) {
                    $_SESSION['employeeId'] = $result[0]['id'];
                    $_SESSION['name'] = $result[0]['name'];
                    $_SESSION['login'] = $result[0]['login'];
                    $_SESSION['permission_level'] = $result[0]['permission_level'];
                    http_response_code(200);
                    header('Location: /../view/admin.php');
                    exit();
                } else {
                    http_response_code(401);
                }
                
            }
        }

        public function registerNewEmployee($permissionLevel, $employeeArray) {
            if ($permissionLevel > 1 && !is_null($permissionLevel)) {
                if (!$this->isValidFields($employeeArray)) {
                    return 400;
                }

                if ($employeeArray['password'] == $employeeArray['confirmPassword']) {
                    $employeeDAO = new EmployeeDAO();
                    return $employeeDAO->registerNewEmployee($employeeArray);
                } else {
                    return 400;
                }
            } else {
                return 401;
            }
        }

        public function updateEmployee($permissionLevel, $employeeArray, $id) {
            if ($permissionLevel > 0 && !is_null($permissionLevel)) {
                if (!$this->isValidFields($employeeArray)) {
                    return 400;
                }

                if (isset($employeeArray['password']) && !$this->isValidPassword($employeeArray['password'], $employeeArray['confirm_password'])) {
                    return 400;
                }

                $employeeDAO = new EmployeeDAO();
                $employeeArray['id'] = $id;
                return $employeeDAO->updateEmployee($employeeArray);
            } else {
                return 401;
            }
        }

        public function deleteEmployee($permissionLevel, $id) {
            if ($permissionLevel > 2) {
                if (isset($id) && !is_null($id) && !empty($id)) {
                    $employeeDAO = new EmployeeDAO();
                    $employeeDAO->deleteEmployee($id);
                    return 200;
                } else {
                    return 400;
                }
            } else {
                return 401;
            }
        }

        public function getAllEmployee($permissionLevel) {
            if ($permissionLevel > 0 && !is_null($permissionLevel)) {
                $employeeDAO = new EmployeeDAO();
                return $employeeDAO->getAllEmployee($permissionLevel);
            }
        }

        public function getEmployee($id) {
            if ($id > 0 && !is_null($id)) {
                $employeeDAO = new EmployeeDAO();
                return $employeeDAO->getEmployee($id);
            }
        }

        public function logout() {
            unset($_SESSION['employeeId']);
            unset($_SESSION['name']);
            unset($_SESSION['login']);
            unset($_SESSION['permission_level']);

            header('Location: ../index.php');
            exit();
        }

        private function isValidLogin($arrayLogin) {
            print_r($arrayLogin);
            if (isset($arrayLogin['login']) && !is_null($arrayLogin['login']) && isset($arrayLogin['login']) && !is_null($arrayLogin['login'])) {
                return true;
            } 
            return false;
        }

        private function isValidPassword($password, $confirmPassword) {
            if (is_null($password) || empty($password) || $password != $confirmPassword) {
                return false;
            }
            return true;
        }

        private function isValidFields($arrayFields) {
            foreach($arrayFields as $value) {
                if (is_null($value) || empty($value)) {
                    return false;
                }
            }
            return true;
        }
    }

?>