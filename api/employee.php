<?php
    session_start();

    if (!isset($_SESSION['employeeId']) || is_null($_SESSION['employeeId']) || $_SESSION['permission_level'] <= 1) {
        header('Location: ../index.php');
        exit();
    }

    require_once __DIR__ . '/../controller/EmployeeController.php';
    header('Content-Type: application/json');

    switch($_SERVER['REQUEST_METHOD']) {

        case 'GET': {
            if (!array_key_exists('id', $_GET)) {
                http_response_code(200);
                $employeeController = new EmployeeController();
                echo json_encode($employeeController->getAllEmployee($_SESSION['permission_level']));
            } else {
                http_response_code(200);
                $employeeController = new EmployeeController();
                echo json_encode($employeeController->getEmployee($_GET['id']));
            }

        } break;

        case 'POST': {

            if (!array_key_exists('id', $_GET)) {
                $employeeController = new EmployeeController();
                $statusCode = $employeeController->registerNewEmployee($_SESSION['permission_level'], $_POST);
                http_response_code($statusCode);
            } else {
                $employeeController = new EmployeeController();
                $statusCode = $employeeController->updateEmployee($_SESSION['permission_level'], $_POST, $_GET['id']);
                http_response_code($statusCode);
            }
        } break;

        case 'DELETE': {
            $employeeController = new EmployeeController();
            $statusCode = $employeeController->deleteEmployee($_SESSION['permission_level'], $_GET['id']);
            http_response_code($statusCode);
        } break;
    }

?>