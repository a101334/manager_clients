<?php
    session_start();

    if (!isset($_SESSION['employeeId']) || is_null($_SESSION['employeeId'])) {
        header('Location: ../index.php');
        exit();
    }

    require_once __DIR__ . '/../controller/AddressController.php';
    header('Content-Type: application/json');

    switch($_SERVER['REQUEST_METHOD']) {

        case 'GET': {
            if (!array_key_exists('cod', $_GET)) {
                http_response_code(401);
            } else {
                http_response_code(200);
                $addressController = new AddressController();
                echo json_encode($addressController->getAllAddressCustomer($_GET['cod']));
            }

        } break;

        case 'POST': {

            if (isset($_GET['cod']) && !is_null($_GET['cod'])) {
                $addressController = new AddressController();
                $statusCode = $addressController->registerNewAddressCustomer($_POST);
                http_response_code($statusCode);
            } else if(isset($_GET['id']) && !is_null($_GET['id'])) {
                $addressController = new AddressController();
                $statusCode = $addressController->updateAddress($_POST, $_GET['id']);
                http_response_code($statusCode);
            }
        } break;

        case 'DELETE': {
            $addressController = new AddressController();
            $statusCode = $addressController->deleteAddress($_GET['id']);
            http_response_code($statusCode);
        } break;
    }

?>