<?php
    session_start();

    if (!isset($_SESSION['employeeId']) || is_null($_SESSION['employeeId'])) {
        header('Location: ../index.php');
        exit();
    }

    require_once __DIR__ . '/../controller/CustomerController.php';
    header('Content-Type: application/json');

    switch($_SERVER['REQUEST_METHOD']) {

        case 'GET': {
            if (!array_key_exists('cod', $_GET)) {
                http_response_code(200);
                $customerController = new CustomerController();
                echo json_encode($customerController->getAllCustomer());
            } else {
                http_response_code(200);
                $customerController = new CustomerController();
                // echo json_encode($customerController->getCustomer($_GET['cod']));
            }

        } break;

        case 'POST': {

            if (!array_key_exists('cod', $_GET)) {
                $customerController = new CustomerController();
                $statusCode = $customerController->registerNewCustomer($_POST);
                http_response_code($statusCode);
            } else {
                $customerController = new CustomerController();
                $statusCode = $customerController->updateCustomer($_POST, $_GET['cod']);
                http_response_code($statusCode);
            }
        } break;

        case 'DELETE': {
            $customerController = new CustomerController();
            $statusCode = $customerController->deleteCustomer($_GET['cod']);
            http_response_code($statusCode);
        } break;
    }

?>