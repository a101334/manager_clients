DROP DATABASE IF EXISTS coorporation ;
CREATE DATABASE coorporation;

USE coorporation;

DROP TABLE IF EXISTS coorporation.employee ;
CREATE TABLE coorporation.employee (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    login VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL,
    permission_level ENUM ('1', '2', '3') DEFAULT '1',
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS coorporation.customer ;
CREATE TABLE customer (
    cod INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    birthday DATETIME NOT NULL,
    rg VARCHAR(20) NOT NULL UNIQUE,
    cpf VARCHAR(20) NOT NULL UNIQUE,
    phone VARCHAR(30) NOT NULL,
    PRIMARY KEY (cod)
);

DROP TABLE IF EXISTS address ;
CREATE TABLE address (
    id INT NOT NULL AUTO_INCREMENT,
    cod_customer INT NOT NULL,
    cep VARCHAR(20) NOT NULL,
    public_place VARCHAR(100) NOT NULL,
    number INT NOT NULL,
    complement VARCHAR(100),
    neighborhood VARCHAR(100) NOT NULL,
    locality VARCHAR(100) NOT NULL,
    uf CHAR(2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cod_customer) REFERENCES customer(cod) ON DELETE CASCADE
);

INSERT INTO coorporation.employee (name, login, password, permission_level) VALUES 
('administrator', 'admin', MD5('admin'), 3),
('alex', 'alex', MD5('alex'), 1),
('rafael', 'rafael', MD5('rafael'), 2);

INSERT INTO coorporation.customer (name, birthday, rg, cpf, phone) VALUES 
('joão', '1991-02-15', '11111111111', '2222222222', '(19) 8423-3848'),
('Mary', '1996-12-05', '333333333', '4444444444', '(17) 9223-1832');

INSERT INTO coorporation.address (cod_customer, cep, public_place, number, complement, neighborhood, locality, uf) VALUES
(1, '01001000', 'Praça da Sé', 12, 'lado ímpar', 'Sé', 'São Paulo', 'SP'),
(1, '88330012', 'Avenida Atlântica', 135, 'de 1551/1552 a 2069/2070', 'Centro', 'Balneário Camboriú', 'SC'),
(2, '13414900', 'Avenida Limeira 722', 503, '', 'Areião', 'Piracicaba', 'SP');