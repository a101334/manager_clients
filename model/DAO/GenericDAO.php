<?php
    require_once __DIR__ .'/../config.php';

    class GenericDAO {
        protected $conn;

        public function getConnection() {
            try {
                $this->conn = new PDO('mysql:host='.DB_HOST, DB_USER, DB_PASSWORD);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
        }

        public function closeConnection() {
            if(!is_null($this->conn)) {
                $this->conn = NULL;
            }
        }
    }
?>