<?php
    require_once __DIR__ . '/GenericDAO.php';

    class EmployeeDAO extends GenericDAO {

        public function registerNewEmployee($employeeArray) {
            
            try {
                $this->getConnection();

                $sqlInser = 'INSERT INTO coorporation.employee (name, login, password, permission_level) VALUES (:name, :login, MD5(:password), :permission_level)';

                $stm = $this->conn->prepare($sqlInser);

                $stm->bindValue(':name', $employeeArray['name']);
                $stm->bindValue(':login', $employeeArray['login']);
                $stm->bindValue(':password', $employeeArray['password']);
                $stm->bindValue(':permission_level', $employeeArray['permission_level']);

                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function getAllEmployee($permissionLevel) {

            try {
                $this->getConnection();

                $sqlQuery = 'SELECT id, name, login, permission_level FROM coorporation.employee WHERE permission_level <= :permission_level';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindValue(':permission_level', $permissionLevel);
                $stm->execute();
                $this->closeConnection();
                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function getEmployee($id) {

            try {
                $this->getConnection();

                $sqlQuery = 'SELECT id, name, login, permission_level FROM coorporation.employee WHERE id=:id';

                $stm = $this->conn->prepare($sqlQuery);

                $stm->bindValue(':id', $id);
                $stm->execute();
                $this->closeConnection();
                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function updateEmployee($updateEmployeeArray) {
            
            try {
                $this->getConnection();

                $sqlUpdate = 'UPDATE coorporation.employee SET name=:name, login=:login, permission_level=:permission_level WHERE id=:id';

                $stm = $this->conn->prepare($sqlUpdate);

                $stm->bindValue(':name', $updateEmployeeArray['name']);
                $stm->bindValue(':login', $updateEmployeeArray['login']);
                $stm->bindValue(':permission_level', $updateEmployeeArray['permission_level']);
                $stm->bindValue(':id', $updateEmployeeArray['id']);

                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function deleteEmployee($id) {

            try {
                $this->getConnection();

                $sqlDelete = 'DELETE FROM coorporation.employee WHERE id=:id';
                $stm = $this->conn->prepare($sqlDelete);
                $stm->bindValue(':id', $id);
                $stm->execute();
                
                $this->closeConnection();
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function signIn($arrayLogin) {
            try {
                $this->getConnection();

                $sqlQuery = 'SELECT id, name, login, permission_level FROM coorporation.employee WHERE login=:login AND password=MD5(:password)';

                $stm = $this->conn->prepare($sqlQuery);

                $stm->bindValue(':login', $arrayLogin['login']);
                $stm->bindValue(':password', $arrayLogin['password']);

                $stm->execute();
                $this->closeConnection();
                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
        }
    }

?>