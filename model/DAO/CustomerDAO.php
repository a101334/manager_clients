<?php
    require_once __DIR__ . '/GenericDAO.php';

    class CustomerDAO extends GenericDAO {
        public function registerNewCustomer($customerArray) {

            try {
                $this->getConnection();

                $sqlInser = 'INSERT INTO coorporation.customer (name, birthday, rg, cpf, phone) VALUES (:name, :birthday, :rg, :cpf, :phone)';

                $stm = $this->conn->prepare($sqlInser);

                $stm->bindValue(':name', $customerArray['name'], PDO::PARAM_STR);
                $stm->bindValue(':birthday', $customerArray['birthday']);
                $stm->bindValue(':rg', $customerArray['rg'], PDO::PARAM_STR);
                $stm->bindValue(':cpf', $customerArray['cpf'], PDO::PARAM_STR);
                $stm->bindValue(':phone', $customerArray['phone'], PDO::PARAM_STR);

                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function getAllCustomer() {

            try {
                $this->getConnection();

                $sqlQuery = 'SELECT cod, name, birthday, rg, cpf, phone FROM coorporation.customer';
                $stm = $this->conn->prepare($sqlQuery);
                $stm->execute();
                $this->closeConnection();

                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function getCustomer($cod) {

            try {
                $this->getConnection();

                $sqlQuery = 'SELECT cod, name, birthday, rg, cpf, phone FROM coorporation.customer WHERE cod=:cod';

                $stm = $this->conn->prepare($sqlQuery);

                $stm->bindValue(':cod', $cod);
                
                $stm->execute();
                $this->closeConnection();

                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function updateCustomer($updateEmployeeArray) {
            
            try {
                $this->getConnection();

                $sqlUpdate = 'UPDATE coorporation.customer SET name=:name, birthday=:birthday, rg=:rg, cpf=:cpf, phone=:phone  WHERE cod=:cod';

                $stm = $this->conn->prepare($sqlUpdate);

                $stm->bindValue(':name', $updateEmployeeArray['name']);
                $stm->bindValue(':birthday', $updateEmployeeArray['birthday']);
                $stm->bindValue(':rg', $updateEmployeeArray['rg']);
                $stm->bindValue(':cpf', $updateEmployeeArray['cpf']);
                $stm->bindValue(':phone', $updateEmployeeArray['phone']);
                $stm->bindValue(':cod', $updateEmployeeArray['cod']);

                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function deleteCustomer($cod) {

            try {
                $this->getConnection();

                $sqlDelete = 'DELETE FROM coorporation.customer WHERE cod=:cod';
                $stm = $this->conn->prepare($sqlDelete);
                $stm->bindValue(':cod', $cod);
                $stm->execute();
                
                $this->closeConnection();
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }
    }

?>