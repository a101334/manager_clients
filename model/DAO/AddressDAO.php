<?php
    require_once __DIR__ . '/GenericDAO.php';

    class AddressDAO extends GenericDAO {
        public function registerNewAddressCustomer($addressArray) {

            try {
                $this->getConnection();

                $sqlInser = 'INSERT INTO coorporation.address (cod_customer, cep, public_place, number, complement, neighborhood, locality, uf) VALUES (:cod_customer, :cep, :public_place, :number, :complement, :neighborhood, :locality, :uf)';

                $stm = $this->conn->prepare($sqlInser);

                $stm->bindValue(':cod_customer', $addressArray['cod_customer']);
                $stm->bindValue(':cep', $addressArray['cep']);
                $stm->bindValue(':public_place', $addressArray['public_place']);
                $stm->bindValue(':number', $addressArray['number']);
                $stm->bindValue(':complement', $addressArray['complement']);
                $stm->bindValue(':neighborhood', $addressArray['neighborhood']);
                $stm->bindValue(':locality', $addressArray['locality']);
                $stm->bindValue(':uf', $addressArray['uf']);
                
                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function getAllAddressCustomer($customerCod) {

            try {
                $this->getConnection();

                $sqlQuery = 'SELECT * FROM coorporation.address WHERE cod_customer=:cod';

                $stm = $this->conn->prepare($sqlQuery);

                $stm->bindValue(':cod', $customerCod);

                $stm->execute();
                $this->closeConnection();

                return $stm->fetchAll(PDO::FETCH_ASSOC);
                
                
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }

        public function updateAddressCustomer($updateAddressArray) {
            
            try {
                $this->getConnection();

                $sqlUpdate = 'UPDATE coorporation.address SET cod_customer=:cod_customer, cep=:cep, public_place=:public_place, number=:number, complement=:complement, neighborhood=:neighborhood, locality=:locality, uf=:uf  WHERE id=:id';

                $stm = $this->conn->prepare($sqlUpdate);

                $stm->bindValue(':cod_customer', $updateAddressArray['cod_customer']);
                $stm->bindValue(':cep', $updateAddressArray['cep']);
                $stm->bindValue(':public_place', $updateAddressArray['public_place']);
                $stm->bindValue(':number', $updateAddressArray['number']);
                $stm->bindValue(':complement', $updateAddressArray['complement']);
                $stm->bindValue(':neighborhood', $updateAddressArray['neighborhood']);
                $stm->bindValue(':locality', $updateAddressArray['locality']);
                $stm->bindValue(':uf', $updateAddressArray['uf']);
                $stm->bindValue(':id', $updateAddressArray['id']);

                $stm->execute();
                $this->closeConnection();
                return 200;
            } catch(PDOException $e) {
                echo $e->getMessage().'<br>';
                return 500;
            }
        }

        public function deleteAddressCustomer($id) {

            try {
                $this->getConnection();

                $sqlDelete = 'DELETE FROM coorporation.address WHERE id=:id';
                $stm = $this->conn->prepare($sqlDelete);
                $stm->bindValue(':id', $id);
                $stm->execute();
                
                $this->closeConnection();
            } catch (PDOException $e) {
                echo $e->getMessage().'<br>';
            }
            
        }
    }

?>