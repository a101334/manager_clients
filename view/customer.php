<?php
    session_start();

    if (!isset($_SESSION['employeeId']) || is_null($_SESSION['employeeId'])) {
        header('Location: ../index.php');
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/customer.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Coorporation</title>
</head>
<body>
    <?php include __DIR__ . '/header.php' ?>    
    <?php
        if ($_SESSION['permission_level'] > 1) {
            include __DIR__ . '/menu.php'; 
        } 
    ?>

    <h1>Manage Customer</h1>
    <button id="btn-new-customer" class="btn btn-primary">New Customer</button>
    <section id="customer-section">
    
    </section>

    <!-- The Modal -->
    <div id="modal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <section id='modal-customer-content'>

            </section>
        </div>
    </div>

    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/functions.js"></script>
    <script src="../js/address.js"></script>
    <script src="../js/customer.js"></script>
</body>
</html>