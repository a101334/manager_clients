<?php
    require_once __DIR__ . '/../controller/EmployeeController.php';
    $employeeController = new EmployeeController();

    if(!array_key_exists('logout', $_POST)) {
        $employeeController->tryLogin($_POST);
    } else {
        $employeeController->logout();
    }
    
?>