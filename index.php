<?php
    session_start();
    require_once __DIR__ . '/model/config.php';

    if (isset($_SESSION['employeeId']) && !is_null($_SESSION['employeeId'])) {
        if ($_SESSION['permission_level'] > 1) {
            header('Location: view/admin.php');
            exit();
        } else {
            header('Location: view/customer.php');
            exit();
        }
        
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/index.css">
    <title>Manager Customers</title>
</head>
<body>
    <section class='login-container'>
       
        <h1>Login</h1>
        <div>
            <input type="text" id='login' placeholder="login" value="admin">
            <input type="text" id='password' placeholder="password" value="admin">
            <button id='sign-in' class='btn btn-primary'>Sign In</button>
        </div>
        
    </section>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/script.js"></script>
</body>
</html>