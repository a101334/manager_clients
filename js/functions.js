const logout = () => {
    const data = {
        logout: 'logout'
    }

    $.post(`${baseUrlLogin}`, data, (res, status) => {
        window.location.reload();
    });
}

const createPInfoCustomer = (divElement, labelName, value) => {
    const p = document.createElement('p');
    p.innerHTML = `<span>${labelName} </span>${value}`;
    divElement.appendChild(p);    
}

const createRowInfoCustomer = (tableElement, labelName, value) => {
    const tr = document.createElement('tr');

    const tdLable = document.createElement('td');
    tdLable.innerHTML = `<strong>${labelName}<strong>`;
    tr.appendChild(tdLable);

    const tdValue = document.createElement('td');
    tdValue.innerHTML = `<strong>${value}<strong>`;
    tr.appendChild(tdValue);

    tableElement.appendChild(tr);    
}

const createInputCustomer = (formElement, typeInput, id, placeHolder, value) => {
    const inputElement = document.createElement('input');
    inputElement.type = typeInput;
    inputElement.id = id;
    inputElement.placeholder = placeHolder;
    inputElement.value = value;

    formElement.appendChild(inputElement);
}

const onlyNumbers = (event) => {
    event.target.value = event.target.value.replace(/[^\d]/g,'');
}

const messageError = (statusCode) => {
    console.log(statusCode);
    switch(statusCode) {
        case 400 : {
            alert('Missing/Invalid parameters. All fields must be filled.');
        } break;

        case 401 : {
            alert('Permission denied.');
        } break;

        case 500 : {
            alert('Internal Server Error');
        } break;
    }    
}

const generateTemplateAddressUpdate = (codCustomer, dataCustomerAddress, elementModalContainer) => {
    if (elementModalContainer.childNodes.length > 2) {
        elementModalContainer.removeChild(elementModalContainer.childNodes[2]);
    }

    if (dataCustomerAddress.length > 0) {
        const sectionAddress = document.createElement('section');
        sectionAddress.innerHTML = '';
        sectionAddress.id = `section-address-customer-${codCustomer}`;

        const titleInfoAddress = document.createElement('h1');
        titleInfoAddress.innerHTML = 'Customer Update Addresses';
        sectionAddress.appendChild(titleInfoAddress);

        for(i = 0; i < dataCustomerAddress.length; i++) {
            const articleFormAddress = document.createElement('article');
            articleFormAddress.classList.add('form-address');
            const id = dataCustomerAddress[i].id;
            createInputCustomer (articleFormAddress, 'text', `address-cod-customer-${id}`, 'Cod customer', dataCustomerAddress[i].cod_customer);
            createInputCustomer (articleFormAddress, 'text', `address-cep-${id}`, 'CEP', dataCustomerAddress[i].cep);
            createInputCustomer (articleFormAddress, 'text', `address-public-place-${id}`, 'Public Place', dataCustomerAddress[i].public_place);
            createInputCustomer (articleFormAddress, 'text', `address-number-${id}`, 'N°', dataCustomerAddress[i].number);
            createInputCustomer (articleFormAddress, 'text', `address-complement-${id}`, 'Complement', dataCustomerAddress[i].complement);
            createInputCustomer (articleFormAddress, 'text', `address-neighborhood-${id}`, 'Neighborhood', dataCustomerAddress[i].neighborhood);
            createInputCustomer (articleFormAddress, 'text', `address-locality-${id}`, 'Locality', dataCustomerAddress[i].locality);
            createInputCustomer (articleFormAddress, 'text', `address-uf-${id}`, 'UF', dataCustomerAddress[i].uf);
            
            articleFormAddress.childNodes[1].addEventListener('keyup', onlyNumbers);
            articleFormAddress.childNodes[3].addEventListener('keyup', onlyNumbers);

            const btnUpdateAddress = document.createElement('button');
            btnUpdateAddress.classList.add('btn', 'btn-primary')
            btnUpdateAddress.innerHTML = 'Update';
            btnUpdateAddress.addEventListener('click', () => {
                updateAddress(id)
            });
            articleFormAddress.appendChild(btnUpdateAddress);

            const btnDeleteAddress = document.createElement('button');
            btnDeleteAddress.classList.add('btn', 'btn-danger')
            btnDeleteAddress.innerHTML = 'Delete';
            btnDeleteAddress.addEventListener('click', () => {
                deleteAddress(id, codCustomer);
            });
            articleFormAddress.appendChild(btnDeleteAddress);

            sectionAddress.appendChild(articleFormAddress);
        }
        elementModalContainer.appendChild(sectionAddress);
    } 
}