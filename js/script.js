$(document).ready(function(){

    document.querySelector('#sign-in').addEventListener('click',signIn);

});

const signIn = () => {
    const login = document.querySelector('#login').value;
    const password = document.querySelector('#password').value;

    const data = {
        login,
        password
    };

    $.post('view/login.php', data, (data, status) => {
        window.location.reload();
    })
    .then((res, status) => {

    })
    .catch(err => {
        messageError(err.status);
    });
};

