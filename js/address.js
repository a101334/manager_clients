const getAllAddress = async (codCustomer) => {
    return await $.get(`${baseUrlAddressApi}?cod=${codCustomer}`, (res, status) => {
        console.log(status);
        console.log(res);    
    })
    .then((res, status) => {
        return res;
    })
    .catch(err => { 
        messageError(err.status);
    });
}

const getAddressByCEP = async () => {
    const cep = document.querySelector('#address-cep').value;
    
    if (cep.length >= 7) {
        $.get(`https://viacep.com.br/ws/${cep}/json/`, (res, status) => {
            console.log(status);
            console.log(res);
            document.querySelector(`#address-public-place`).value = res.logradouro;
            document.querySelector(`#address-complement`).value = res.complemento;
            document.querySelector(`#address-neighborhood`).value = res.bairro;
            document.querySelector(`#address-locality`).value = res.localidade;
            document.querySelector(`#address-uf`).value = res.uf;
        });
    }
    
}

const registerAddress = (cod) => {

    const cod_customer = cod;
    const cep = document.querySelector(`#address-cep`).value;
    const public_place = document.querySelector(`#address-public-place`).value;
    const number = document.querySelector(`#address-number`).value;
    const complement = document.querySelector(`#address-complement`).value;
    const neighborhood = document.querySelector(`#address-neighborhood`).value;
    const locality = document.querySelector(`#address-locality`).value;
    const uf = document.querySelector(`#address-uf`).value;

    const data = {
        cod_customer,
        cep,
        public_place,
        number,
        complement,
        neighborhood,
        locality,
        uf
    }

    $.post(`${baseUrlAddressApi}?cod=${cod}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then(async (res, status) => {
        const allAddressCustomer = await getAllAddress(cod);

        if (allAddressCustomer.length > 0) {
            const modalContainer = document.querySelector('#modal-customer-content');
            generateTemplateAddressUpdate (cod, allAddressCustomer, modalContainer);
        }
    })
    .catch(async err => { 
        if (err.status == 200) {
            const allAddressCustomer = await getAllAddress(cod);

            if (allAddressCustomer.length > 0) {
                const modalContainer = document.querySelector('#modal-customer-content');
                generateTemplateAddressUpdate (cod, allAddressCustomer, modalContainer);
            }
        } else {
            messageError(err.status);
        }
    });
}

const updateAddress = (idAddress) => {

    const cod_customer = document.querySelector(`#address-cod-customer-${idAddress}`).value;
    const cep = document.querySelector(`#address-cep-${idAddress}`).value;
    const public_place = document.querySelector(`#address-public-place-${idAddress}`).value;
    const number = document.querySelector(`#address-number-${idAddress}`).value;
    const complement = document.querySelector(`#address-complement-${idAddress}`).value;
    const neighborhood = document.querySelector(`#address-neighborhood-${idAddress}`).value;
    const locality = document.querySelector(`#address-locality-${idAddress}`).value;
    const uf = document.querySelector(`#address-uf-${idAddress}`).value;

    const data = {
        cod_customer,
        cep,
        public_place,
        number,
        complement,
        neighborhood,
        locality,
        uf
    }
    
    $.post(`${baseUrlAddressApi}?id=${idAddress}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then((res, status) => {
        console.log(status);
    })
    .catch(err => { 
        messageError(err.status);
    });
}

const deleteAddress = (id, cod) => {
    //reference: https://dev.to/gyi2521/ajax---get-post-putand-delete--m9j
    $.ajax({ url: `${baseUrlAddressApi}?id=${id}`, method: "DELETE" })
    .then(async (res, status) => {
        const allAddressCustomer = await getAddress(id);
        const modalContainer = document.querySelector('#modal-customer-content');
        generateTemplateAddressUpdate (cod, allAddressCustomer, modalContainer);
        
    })
    .catch(async err => { 
        if (err.status == 200) {
            const allAddressCustomer = await getAllAddress(cod);
            const modalContainer = document.querySelector('#modal-customer-content');
            generateTemplateAddressUpdate (cod, allAddressCustomer, modalContainer);
            
        } else {
            messageError(err.status);
        }
    });
}