$(document).ready(function(){
    
    document.querySelector('#logout').addEventListener('click', logout);
    document.querySelector('.close').addEventListener('click', closeModal);
    document.querySelector('#btn-new-employee').addEventListener('click', () => {
        const btnRegister = document.createElement('button');
        btnRegister.innerHTML = 'Register';
        btnRegister.classList.add('btn', 'btn-primary');
        btnRegister.addEventListener('click', registerEmployee);
        openModalManage(btnRegister, '');
    });
    getAllEmployee();
});

const baseUrlEmployeeApi = '../api/employee.php';
const baseUrlLogin = 'login.php';

const getAllEmployee = () => {
    $.get(`${baseUrlEmployeeApi}`, (res, status) => {
        const employees = res;

        const sectionContent = document.querySelector('#employees-section');
        sectionContent.innerHTML = '';
        employees.forEach(employee => {

            const article = document.createElement('article');
            article.classList.add('customer-info');

            const divInfo = document.createElement('div');

            createPInfoCustomer (divInfo, 'ID:', employee.id);
            createPInfoCustomer (divInfo, 'NAME:', employee.name);
            createPInfoCustomer (divInfo, 'LOGIN:', employee.login);
            createPInfoCustomer (divInfo, 'PERMISSION LEVEL:', employee.permission_level);

            article.appendChild(divInfo);

            const divBtnGroup = document.createElement('div');
            divBtnGroup.classList.add('btn-group-customer');

            const btnInfo = document.createElement('button');
            btnInfo.classList.add('btn', 'btn-info');
            btnInfo.innerHTML = 'info';

            btnInfo.addEventListener('click', () => {
                openModalInfo(employee);
            });

            divBtnGroup.appendChild(btnInfo);
            
            const btnUpdate = document.createElement('button');
            btnUpdate.classList.add('btn', 'btn-primary');
            btnUpdate.innerHTML = 'update';
            btnUpdate.addEventListener('click', () => {
                const btnUpdateModal = document.createElement('button');
                btnUpdateModal.classList.add('btn', 'btn-primary');
                btnUpdateModal.innerHTML = 'Update';
                btnUpdateModal.addEventListener('click', () => {
                    updateEmployee(employee.id);
                });
                openModalManage(btnUpdateModal, employee);
            });
            
            divBtnGroup.appendChild(btnUpdate);

            const btnDelete = document.createElement('button');
            btnDelete.classList.add('btn', 'btn-danger');
            btnDelete.innerHTML = 'delete';
            btnDelete.addEventListener('click', () => {
                deleteEmployee(employee.id);
            });
            divBtnGroup.appendChild(btnDelete);

            article.appendChild(divBtnGroup);

            sectionContent.appendChild(article);
        });
    });
}

const registerEmployee = () => {

    const name = document.querySelector('#employeeName').value;
    const login = document.querySelector('#employeeLogin').value;
    const permission_level = document.querySelector('#employeePermissionLevel').value;
    const password = document.querySelector('#employeePassword').value;
    const confirmPassword = document.querySelector('#employeeConfirmPassword').value;
    
    if (password != confirmPassword) {
        console.log('password != confirmPassword');
        return;
    }

    const data = {
        name,
        login,
        permission_level,
        password,
        confirmPassword
    }

    $.post(`${baseUrlEmployeeApi}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then((res, status) => {
        getAllEmployee();
    })
    .catch(err => { 
        messageError(err.status);
        getAllEmployee();
    });
}

const updateEmployee = () => {
    const id = document.querySelector('#employeeId').value;
    const name = document.querySelector('#employeeName').value;
    const login = document.querySelector('#employeeLogin').value;
    const permission_level = document.querySelector('#employeePermissionLevel').value;

    const data = {
        name,
        login,
        permission_level
    }

    $.post(`${baseUrlEmployeeApi}?id=${id}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then((res, status) => {
        console.log(status);
        getAllEmployee(); 
    })
    .catch(err => { 
        messageError(err.status);
        getAllEmployee(); 
    });
}

const deleteEmployee = (id) => {
    //reference: https://dev.to/gyi2521/ajax---get-post-putand-delete--m9j
    $.ajax({ url: `${baseUrlEmployeeApi}?id=${id}`, method: "DELETE" })
            .then((res, status) => {
        console.log(status);
        getAllEmployee();
    })
    .catch(err => { 
        messageError(err.status);
        getAllEmployee();
    });
}

const openModalManage = (btnModal, data) => {
    const modal = document.querySelector('#modal');
    const formModal = document.querySelector('#formModal');
    formModal.innerHTML = '';
    let countChildNodeId = 3;
    const titleModal = document.createElement('h1');
    titleModal.innerHTML = 'Employee';
    formModal.appendChild(titleModal);

    if (data && data.id && data.id != '') {
        createInputCustomer (formModal, 'text', 'employeeId', 'ID', data.id);
        countChildNodeId ++;
    }

    createInputCustomer (formModal, 'text', 'employeeName', 'Employee name', data ? data.name : '');
    createInputCustomer (formModal, 'text', 'employeeLogin', 'Login', data ? data.login : '');
    createInputCustomer (formModal, 'text', 'employeePermissionLevel', 'Permission Level', data ? data.permission_level : '');

    formModal.childNodes[countChildNodeId].addEventListener('keyup', onlyNumbers);

    if (btnModal && btnModal != '' && btnModal.innerHTML != 'Update'){
        createInputCustomer (formModal, 'password', 'employeePassword', 'Password', '');
        createInputCustomer (formModal, 'password', 'employeeConfirmPassword', 'Confirm Password', '');
    }

    formModal.appendChild(btnModal);

    modal.style.display = 'block';
}

const openModalInfo = (dataEmployee) => {
    const modal = document.querySelector('#modal');
    const articleEmployee = document.createElement('article');
    const modalContainer = document.querySelector('#formModal');
    modalContainer.innerHTML = '';

    const titleEmployeeInfo = document.createElement('h1');
    titleEmployeeInfo.innerHTML = 'EMPLOYEE INFO';
    articleEmployee.appendChild(titleEmployeeInfo);

    const tableEmployee = document.createElement('table');

    createRowInfoCustomer (tableEmployee,'ID', dataEmployee.id);
    createRowInfoCustomer (tableEmployee,'NAME', dataEmployee.name);
    createRowInfoCustomer (tableEmployee,'LOGIN', dataEmployee.login);
    createRowInfoCustomer (tableEmployee,'PERMISSION LEVEL', dataEmployee.permission_level);

    articleEmployee.appendChild(tableEmployee);
    
    modalContainer.appendChild(articleEmployee);
    modal.style.display = 'block';
}

const closeModal = () => {
    const modal = document.querySelector('#modal');
    modal.style.display = 'none';
    getAllEmployee();
}

