$(document).ready(function(){
    
    document.querySelector('#logout').addEventListener('click', logout);
    document.querySelector('.close').addEventListener('click', closeModal);
    document.querySelector('#btn-new-customer').addEventListener('click', () => {
        const btnRegisterCustomer = document.createElement('button');
        btnRegisterCustomer.classList.add('btn', 'btn-primary');
        btnRegisterCustomer.innerHTML = 'Register';
        btnRegisterCustomer.addEventListener('click', registerCustomer);
        openModalManage(btnRegisterCustomer, '', []);
    });
    getAllCustomer();
});

const baseUrlCustomerApi = '../api/customer.php';
const baseUrlAddressApi = '../api/address.php';
const baseUrlLogin = 'login.php';

const getAllCustomer = () => {
    $.get(`${baseUrlCustomerApi}`, (res, status) => {
        const customers = res;
        
        const sectionContent = document.querySelector('#customer-section');
        sectionContent.innerHTML = '';
        customers.forEach( async (customer) => {
            //convert birthday date string in date type
            const customerBirthday = new Date(customer.birthday);
        
            const article = document.createElement('article');
            article.classList.add('customer-info');

            const divInfo = document.createElement('div');

            createPInfoCustomer (divInfo, 'COD:', customer.cod);
            createPInfoCustomer (divInfo, 'NAME:', customer.name);
            createPInfoCustomer (divInfo, 'BIRTHDAY:', customerBirthday.toLocaleDateString('pt-BR'));
            createPInfoCustomer (divInfo, 'RG:', customer.rg);
            createPInfoCustomer (divInfo, 'CPF:', customer.cpf);
            createPInfoCustomer (divInfo, 'PHONE:', customer.phone);

            article.appendChild(divInfo);

            const divBtnGroup = document.createElement('div');
            divBtnGroup.classList.add('btn-group-customer');

            const btnInfo = document.createElement('button');
            btnInfo.classList.add('btn', 'btn-info');
            btnInfo.innerHTML = 'info';

            const allAddress = await getAllAddress(customer.cod, sectionContent);

            btnInfo.addEventListener('click', () => {
                
                openModalInfo(customer, allAddress);
            });
            divBtnGroup.appendChild(btnInfo);
            
            const btnUpdate = document.createElement('button');
            btnUpdate.classList.add('btn', 'btn-primary');
            btnUpdate.innerHTML = 'update';
            btnUpdate.addEventListener('click', () => {
                const btnUpdateModal = document.createElement('button');
                btnUpdateModal.classList.add('btn', 'btn-primary');
                btnUpdateModal.innerHTML = 'Update';
                btnUpdateModal.addEventListener('click', () => {
                    updateCustomer(customer.cod);
                });
                openModalManage(btnUpdateModal, customer, allAddress);
            });
            
            divBtnGroup.appendChild(btnUpdate);

            const btnDelete = document.createElement('button');
            btnDelete.classList.add('btn', 'btn-danger');
            btnDelete.innerHTML = 'delete';
            btnDelete.addEventListener('click', () => {
                deleteCustomer(customer.cod);
            });
            divBtnGroup.appendChild(btnDelete);

            article.appendChild(divBtnGroup);

            sectionContent.appendChild(article);

        });
    });
}

const registerCustomer = () => {

    const name = document.querySelector('#customerName').value;
    const birthday = document.querySelector('#customerBirthday').value;
    const rg = document.querySelector('#customerRG').value;
    const cpf = document.querySelector('#customerCPF').value;
    const phone = document.querySelector('#customerPhone').value;

    const data = {
        name,
        birthday,
        rg,
        cpf,
        phone
    }

    $.post(`${baseUrlCustomerApi}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then((res, status) => {
        console.log(status);
        getAllCustomer();
    })
    .catch(err => { 
        messageError(err.status);
        getAllCustomer();
    });
}

const updateCustomer = () => {
    const cod = document.querySelector('#customerCod').value;
    const name = document.querySelector('#customerName').value;
    const birthday = document.querySelector('#customerBirthday').value;
    const rg = document.querySelector('#customerRG').value;
    const cpf = document.querySelector('#customerCPF').value;
    const phone = document.querySelector('#customerPhone').value;

    const data = {
        name,
        birthday,
        rg,
        cpf,
        phone
    }

    $.post(`${baseUrlCustomerApi}?cod=${cod}`, data, (res, status) => {
        console.log(status);
        console.log(res);
    })
    .then((res, status) => {
        console.log(status);
        getAllCustomer();
    })
    .catch(err => { 
        messageError(err.status);
        getAllCustomer();
    });
}

const deleteCustomer = (cod) => {
    //reference: https://dev.to/gyi2521/ajax---get-post-putand-delete--m9j
    $.ajax({ url: `${baseUrlCustomerApi}?cod=${cod}`, method: "DELETE" })
    .then((res, status) => {
        console.log(status);
        getAllCustomer();
    })
    .catch(err => { 
        messageError(err.status);
        getAllCustomer();
    });
    
}

const openModalManage = (btnFormCustomer, dataCustomer, dataCustomerAddress) => {
    const modal = document.querySelector('#modal');
    const modalContainer = document.querySelector('#modal-customer-content');
    modalContainer.innerHTML = '';
    let countNodeId = 2;
    const formModal = document.createElement('article');

    const titleInfoCustomer = document.createElement('h1');
    titleInfoCustomer.innerHTML = 'Customer Info';
    formModal.appendChild(titleInfoCustomer);

    if (dataCustomer && dataCustomer.cod && dataCustomer.cod != '') {
        createInputCustomer (formModal, 'text', 'customerCod', 'COD', dataCustomer.cod);
        countNodeId = 4;
    }
    
    createInputCustomer (formModal, 'text', 'customerName', 'Customer Name', dataCustomer ? dataCustomer.name : '');
    createInputCustomer (formModal, 'date', 'customerBirthday', '', dataCustomer ? dataCustomer.birthday.substring(0,10) : '');
    createInputCustomer (formModal, 'text', 'customerRG', 'Customer RG', dataCustomer ? dataCustomer.rg  : '');
    createInputCustomer (formModal, 'text', 'customerCPF', 'Customer CPF', dataCustomer ? dataCustomer.cpf  : '');
    createInputCustomer (formModal, 'text', 'customerPhone', 'Customer Phone', dataCustomer ? dataCustomer.phone  : '');

    formModal.childNodes[countNodeId].addEventListener('keyup', onlyNumbers);
    formModal.childNodes[countNodeId + 1].addEventListener('keyup', onlyNumbers);
    formModal.childNodes[countNodeId + 2].addEventListener('keyup', onlyNumbers);

    modalContainer.appendChild(formModal);
    formModal.appendChild(btnFormCustomer);

    if (dataCustomer) {
        const articleFormNewAddress = document.createElement('article');

        const titleInfoNewAddress = document.createElement('h1');
        titleInfoNewAddress.innerHTML = 'Register New Address';
        articleFormNewAddress.appendChild(titleInfoNewAddress);
    
        createInputCustomer (articleFormNewAddress, 'text', `address-cod-customer`, 'Cod customer', dataCustomer.cod);
        createInputCustomer (articleFormNewAddress, 'text', `address-cep`, 'CEP', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-public-place`, 'Public Place', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-number`, 'N°', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-complement`, 'Complement', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-neighborhood`, 'Neighborhood', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-locality`, 'Locality', '');
        createInputCustomer (articleFormNewAddress, 'text', `address-uf`, 'UF', '');
    
        const btnRegisterAddress = document.createElement('button');
        btnRegisterAddress.classList.add('btn', 'btn-primary');
        btnRegisterAddress.innerHTML = 'Register';
        btnRegisterAddress.addEventListener('click', () => {
            registerAddress(dataCustomer.cod);
        });
    
        articleFormNewAddress.appendChild(btnRegisterAddress);
    
        articleFormNewAddress.childNodes[2].addEventListener('keyup', getAddressByCEP);
        articleFormNewAddress.childNodes[2].addEventListener('keyup', onlyNumbers);
        articleFormNewAddress.childNodes[4].addEventListener('keyup', onlyNumbers);

        modalContainer.appendChild(articleFormNewAddress);
        
        if (dataCustomerAddress.length > 0) {
            generateTemplateAddressUpdate(dataCustomer.cod, dataCustomerAddress, modalContainer);
        } else {
            console.log(`${dataCustomer.name} not have registred address yet.`);
        }
    }

    modal.style.display = 'block';
}

const closeModal = () => {
    const modal = document.querySelector('#modal');
    modal.style.display = 'none';
    getAllCustomer();
}

const openModalInfo = (dataCustomer, dataCustomerAddress) => {
    const modal = document.querySelector('#modal');
    const articleCustomer = document.createElement('article');
    const modalContainer = document.querySelector('#modal-customer-content');

    modalContainer .innerHTML = '';

    const titleCustomerInfo = document.createElement('h1');
    titleCustomerInfo.innerHTML = 'CUSTORMER INFO';
    articleCustomer.appendChild(titleCustomerInfo);

    const tableCustomer = document.createElement('table');

    const localityBirthday = new Date(dataCustomer.birthday);

    createRowInfoCustomer (tableCustomer,'COD', dataCustomer.cod);
    createRowInfoCustomer (tableCustomer,'NAME', dataCustomer.name);
    createRowInfoCustomer (tableCustomer,'BIRTHDAY', localityBirthday.toLocaleDateString());
    createRowInfoCustomer (tableCustomer,'RG', dataCustomer.rg);
    createRowInfoCustomer (tableCustomer,'CPF', dataCustomer.cpf);
    createRowInfoCustomer (tableCustomer,'PHONE', dataCustomer.phone);

    articleCustomer.appendChild(tableCustomer);
    
    if (dataCustomerAddress.length > 0) {
        const titleCustomerAddress = document.createElement('h1');
        titleCustomerAddress.innerHTML = 'CUSTOMER ADDRESS';
        articleCustomer.appendChild(titleCustomerAddress);
    }

    dataCustomerAddress.forEach(address => {
        const tableAddress = document.createElement('table');
        const propertyMapAddress = {
            id: 'ID', 
            cod_customer: 'COD CUSTOMER', 
            cep: 'CEP', 
            public_place: 'PUBLIC PLACE',
            number: 'N°', 
            complement: 'COMPLEMENT', 
            neighborhood: 'NEIGHBORHOOD', 
            locality: 'LOCALITY', 
            uf: 'UF'
        };

        for(key in address) {
            createRowInfoCustomer (tableAddress, propertyMapAddress[key], address[key]);
        }

        articleCustomer.appendChild(tableAddress);
    });

    modalContainer.appendChild(articleCustomer);
    modal.style.display = 'block';
}


